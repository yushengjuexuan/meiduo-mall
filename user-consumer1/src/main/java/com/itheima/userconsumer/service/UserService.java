package com.itheima.userconsumer.service;

import com.itheima.b2b.commonmodule.model.User;
import com.itheima.userconsumer.hystrix.UserServiceHystrix;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Component  //Spring框架中的一个注解，当Spring扫描到这个注解时，会自动将这个类实例化为一个Bean，并加入到Spring容器中
@FeignClient(value = "user-provider", fallback = UserServiceHystrix.class)
//@FeignClient(value = "user-provider")
public interface UserService {
    @RequestMapping(value = "/login",method = RequestMethod.GET)
    public User login(@RequestParam(value = "uname") String uname);

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public int register(@RequestParam(value = "upassword") String upassword, @RequestParam(value = "uname") String uname, @RequestParam(value = "usex") String usex);
}
