package com.example.shoppingplation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShoppingPlationApplication {

	public static void main(String[] args) {
		SpringApplication.run(ShoppingPlationApplication.class, args);
	}

}
